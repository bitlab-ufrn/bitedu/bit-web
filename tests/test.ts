import { expect, test } from '@playwright/test';

test('index page has expected beginning h1', async ({ page }) => {
	await page.goto('/');
	await expect(page.getByRole('heading', { name: 'Aqui será a tela inicial de cursos' })).toBeVisible();
});

test('editorjs page index link button send user to expected page', async ({ page }) => {
	await page.goto('/')

	await page.getByRole('link', { name: 'Começe agora a criar seu curso!' }).click();

	await expect(page.getByRole('heading', { name: 'SvelteKit + EditorJS' })).toBeVisible()
})
